/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.hpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 19:27:10 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/14 16:00:51 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRESIDENTIALPARDONFORM_HPP
#define PRESIDENTIALPARDONFORM_HPP

#include "Form.hpp"

using std::string;

class PresidentialPardonForm: public Form
{
	private:
		string _target;

		PresidentialPardonForm();
	public:
		PresidentialPardonForm(const string& target);
		PresidentialPardonForm(const PresidentialPardonForm& presidentialPardonForm);
		~PresidentialPardonForm();
		PresidentialPardonForm& operator=(const PresidentialPardonForm& presidentialPardonForm);
		void execute(const Bureaucrat& executor) const;
		Form* clone() const;
		Form* copy(const string& target) const;
};

#endif
