/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/05 19:58:16 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/11 20:22:02 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
#define BUREAUCRAT_HPP

#include <string>
#include <exception>
#include <ostream>

using std::string; using std::exception;
using std::ostream; using std::endl;

class Form;

class Bureaucrat
{
	private:
		const string _name;
		int _grade;

		Bureaucrat();
	public:
		Bureaucrat(const string& name, int grade);
		Bureaucrat(const Bureaucrat& bureaucrat);
		~Bureaucrat();
		Bureaucrat& operator=(const Bureaucrat& bureaucrat);
		const string& getName() const;
		int getGrade() const;
		void incrementGrade();
		void decrementGrade();
		void signForm(Form& form) const;
		void executeForm(const Form& form) const;

	class GradeTooHighException: public exception
	{
		private:
			string _msg;

			GradeTooHighException();
		public:
			GradeTooHighException(int badGrade);
			~GradeTooHighException() throw() {}
			const char* what() const throw();
	};

	class GradeTooLowException: public exception
	{
		private:
			string _msg;

			GradeTooLowException();
		public:
			GradeTooLowException(int badGrade);
			~GradeTooLowException() throw() {}
			const char* what() const throw();
	};
};

ostream& operator<<(ostream& os, const Bureaucrat& bureaucrat);

#endif
