/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.hpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 12:37:22 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/14 15:59:06 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHRUBBERYCREATIONFORM_HPP
#define SHRUBBERYCREATIONFORM_HPP

#include "Form.hpp"
#include <string>

using std::string;

class ShrubberyCreationForm: public Form
{
	private:
		string _target;

		ShrubberyCreationForm();
	public:
		ShrubberyCreationForm(const string& target);
		ShrubberyCreationForm(const ShrubberyCreationForm& shrubberyCreationForm);
		~ShrubberyCreationForm();
		ShrubberyCreationForm& operator=(const ShrubberyCreationForm& shrubberyCreationForm);
		void execute(const Bureaucrat& executor) const;
		Form* clone() const;
		Form* copy(const string& target) const;
};

#endif
