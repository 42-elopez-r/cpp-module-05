/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/13 14:13:11 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/14 16:10:23 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERN_HPP
#define INTERN_HPP

#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include <string>
#include <exception>

using std::string; using std::exception;

class Intern
{
	private:
		class FormNames
		{
			private:
				Form* _form;
				string* _names;
				int _ammount;

				FormNames();
			public:
				FormNames(Form* form);
				FormNames(const FormNames& formNames);
				~FormNames();
				FormNames& operator=(const FormNames& formNames);
				void addName(const string& name);
				bool hasName(const string& name) const;
				Form* makeForm(const string& target) const;
		};

		FormNames* _forms[3];
	public:
		Intern();
		Intern(const Intern& intern);
		~Intern();
		Intern& operator=(const Intern& intern);
		Form* makeForm(const string& formName, const string& target) const;
};

#endif
