/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 12:44:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/14 16:01:12 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

PresidentialPardonForm::PresidentialPardonForm(const string& target):
	Form("PresidentialPardonForm", 25, 5)
{
	_target = target;
	srand(time(NULL));
}

PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm& presidentialPardonForm):
	Form(presidentialPardonForm)
{
	_target = presidentialPardonForm._target;
	srand(time(NULL));
}

PresidentialPardonForm::~PresidentialPardonForm()
{

}

PresidentialPardonForm&
PresidentialPardonForm::operator=(const PresidentialPardonForm& presidentialPardonForm)
{
	Form::operator=(presidentialPardonForm);
	_target = presidentialPardonForm._target;

	return (*this);
}

void
PresidentialPardonForm::execute(const Bureaucrat& executor) const
{
	if (!isValidExecutor(executor))
		throw GradeTooLowException(executor.getGrade());
	if (!getSigned())
		throw NotSignedException();

	cout << _target << " has been pardoned by Zafod Beeblebrox" << endl;
}

Form*
PresidentialPardonForm::clone() const
{
	return (new PresidentialPardonForm(*this));
}

Form*
PresidentialPardonForm::copy(const string& target) const
{
	return (new PresidentialPardonForm(target));
}
