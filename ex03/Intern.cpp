/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/14 12:45:21 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/14 16:49:25 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"

#include <iostream>

using std::cout; using std::endl;

Intern::Intern()
{
	_forms[0] = new FormNames(new ShrubberyCreationForm(""));
	_forms[0]->addName("ShrubberyCreationForm");
	_forms[0]->addName("ShrubberyCreation");
	_forms[0]->addName("Shrubbery Creation Form");
	_forms[0]->addName("Shrubbery Creation");
	_forms[0]->addName("shrubbery creation form");
	_forms[0]->addName("shrubbery creation");
	_forms[0]->addName("shrubbery_creation_form");
	_forms[0]->addName("shrubbery_creation");

	_forms[1] = new FormNames(new RobotomyRequestForm(""));
	_forms[1]->addName("RobotomyRequestForm");
	_forms[1]->addName("RobotomyRequest");
	_forms[1]->addName("Robotomy Request Form");
	_forms[1]->addName("Robotomy Request");
	_forms[1]->addName("robotomy request form");
	_forms[1]->addName("robotomy request");
	_forms[1]->addName("robotomy_request_form");
	_forms[1]->addName("robotomy_request");

	_forms[2] = new FormNames(new PresidentialPardonForm(""));
	_forms[2]->addName("PresidentialPardonForm");
	_forms[2]->addName("PresidentialPardon");
	_forms[2]->addName("Presidential Pardon Form");
	_forms[2]->addName("Presidential Pardon");
	_forms[2]->addName("presidential pardon form");
	_forms[2]->addName("presidential pardon");
	_forms[2]->addName("presidential_pardon_form");
	_forms[2]->addName("presidential_pardon");
}

Intern::Intern(const Intern& intern)
{
	for (int i = 0; i < 3; i++)
		_forms[i] = NULL;
	*this = intern;
}

Intern::~Intern()
{
	for (int i = 0; i < 3; i++)
		delete _forms[i];
}

Intern&
Intern::operator=(const Intern& intern)
{
	for (int i = 0; i < 3; i++)
		delete _forms[i];

	for (int i = 0; i < 3; i++)
		_forms[i] = new FormNames(*intern._forms[i]);

	return (*this);
}

Form*
Intern::makeForm(const string& formName, const string& target) const
{
	for (int i = 0; i < 3; i++)
		if (_forms[i]->hasName(formName))
			return (_forms[i]->makeForm(target));

	cout << "Form type " << formName << " is unknown" << endl;
	return (NULL);
}

Intern::FormNames::FormNames(Form* form)
{
	_form = form;
	_names = NULL;
	_ammount = 0;
}

Intern::FormNames::FormNames(const FormNames& formNames)
{
	_form = NULL;
	_names = NULL;
	_ammount = 0;
	*this = formNames;
}

Intern::FormNames::~FormNames()
{
	delete _form;
	delete[] _names;
}

Intern::FormNames&
Intern::FormNames::operator=(const FormNames& formNames)
{
	delete _form;
	delete _names;

	_form = formNames._form->clone();
	_ammount = formNames._ammount;

	_names = new string[_ammount];
	for (int i = 0; i < _ammount; i++)
		_names[i] = formNames._names[i];

	return (*this);
}

void
Intern::FormNames::addName(const string& name)
{
	string* newNames;

	if (hasName(name))
		return;

	newNames = new string[_ammount + 1];
	for (int i = 0; i < _ammount; i++)
		newNames[i] = _names[i];
	newNames[_ammount++] = name;

	delete[] _names;
	_names = newNames;
}

bool
Intern::FormNames::hasName(const string& name) const
{
	for (int i = 0; i < _ammount; i++)
		if (_names[i] == name)
			return (true);
	return (false);
}

Form*
Intern::FormNames::makeForm(const string& target) const
{
	return (_form->copy(target));
}
