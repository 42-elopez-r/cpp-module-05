/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 19:12:39 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/14 15:58:22 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
#define FORM_HPP

#include "Bureaucrat.hpp"

#include <string>
#include <ostream>
#include <exception>

using std::string; using std::ostream;
using std::exception;

class Form
{
	private:
		const string _name;
		const int _minGradeSign;
		const int _minGradeExecute;
		bool _isSigned;

		Form();
	protected:
		bool isValidExecutor(const Bureaucrat& executor) const;
	public:
		Form(const string& name, int minGradeSign, int minGradeExecute);
		Form(const Form& form);
		virtual ~Form();
		Form& operator=(const Form& form);
		const string& getName() const;
		int getMinGradeSign() const;
		int getMinGradeExecute() const;
		bool getSigned() const;
		void beSigned(const Bureaucrat& bureaucrat);
		virtual void execute(const Bureaucrat& executor) const = 0;
		virtual Form* clone() const = 0;
		virtual Form* copy(const string& target) const = 0;

	class GradeTooHighException: public exception
	{
		private:
			string _msg;

			GradeTooHighException();
		public:
			GradeTooHighException(int badGrade);
			~GradeTooHighException() throw() {}
			const char* what() const throw();
	};

	class GradeTooLowException: public exception
	{
		private:
			string _msg;

			GradeTooLowException();
		public:
			GradeTooLowException(int badGrade);
			~GradeTooLowException() throw() {}
			const char* what() const throw();
	};

	class NotSignedException: public exception
	{
		public:
			const char* what() const throw();
	};
};

ostream& operator<<(ostream& os, const Form& form);

#endif
