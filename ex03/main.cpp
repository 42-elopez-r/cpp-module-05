/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/12 20:45:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/14 17:12:29 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"
#include <iostream>

using std::cout; using std::endl;

int
main()
{
	Bureaucrat chief("Chief", 1);
	Intern intern, intern_copy;
	Form* gardenForm;
	Form* marvinForm;
	Form* everyoneForm;
	Form* myselfForm;

	cout << "Make some forms:" << endl;
	gardenForm = intern.makeForm("shrubbery creation", "garden");
	marvinForm = intern.makeForm("Robotomy Request Form", "Marvin");
	everyoneForm = intern.makeForm("presidential_pardon", "Everyone");
	cout << *gardenForm << *marvinForm << *everyoneForm;

	cout << endl << "Sign and execute them:" << endl;
	chief.signForm(*gardenForm);
	chief.executeForm(*gardenForm);
	chief.signForm(*marvinForm);
	chief.executeForm(*marvinForm);
	chief.signForm(*everyoneForm);
	chief.executeForm(*everyoneForm);

	cout << endl << "Try to make an invalid form:" << endl;
	intern.makeForm("Inmortality Form", "Myself");

	cout << endl << "Copy intern and try to generate a Robotomy Request form:" << endl;
	intern_copy = intern;
	myselfForm = intern_copy.makeForm("robotomy request", "myself");
	cout << *myselfForm;
	
	delete gardenForm;
	delete marvinForm;
	delete everyoneForm;
	delete myselfForm;
	return (0);
}
