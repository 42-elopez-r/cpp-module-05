/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 12:44:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/14 16:00:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

RobotomyRequestForm::RobotomyRequestForm(const string& target):
	Form("RobotomyRequestForm", 72, 45)
{
	_target = target;
	srand(time(NULL));
}

RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm& robotomyRequestForm):
	Form(robotomyRequestForm)
{
	_target = robotomyRequestForm._target;
	srand(time(NULL));
}

RobotomyRequestForm::~RobotomyRequestForm()
{

}

RobotomyRequestForm&
RobotomyRequestForm::operator=(const RobotomyRequestForm& robotomyRequestForm)
{
	Form::operator=(robotomyRequestForm);
	_target = robotomyRequestForm._target;

	return (*this);
}

void
RobotomyRequestForm::execute(const Bureaucrat& executor) const
{
	if (!isValidExecutor(executor))
		throw GradeTooLowException(executor.getGrade());
	if (!getSigned())
		throw NotSignedException();

	cout << "* Brrrrrrrrrr *" << endl;

	if (rand() % 2 == 0)
		cout << _target << " has been robotomized successfully" << endl;
	else
		cout << _target << "'s robotomization has been a failure, just like Windows 10" << endl;
}

Form*
RobotomyRequestForm::clone() const
{
	return (new RobotomyRequestForm(*this));
}

Form*
RobotomyRequestForm::copy(const string& target) const
{
	return (new RobotomyRequestForm(target));
}
