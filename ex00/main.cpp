/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/05 21:01:20 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/06 20:05:02 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include <iostream>
#include <exception>

using std::cout; using std::endl;
using std::exception;

int
main()
{
	Bureaucrat chief("Chief", 1);
	Bureaucrat that_guy("That guy", 150);
	Bureaucrat* nope;
	Bureaucrat* copy_chief;
	
	cout << chief << that_guy;

	cout << endl << "Incrementing grade:" << endl;
	that_guy.incrementGrade();
	cout << that_guy;

	cout << endl << "Decrementing grade:" << endl;
	that_guy.decrementGrade();
	cout << that_guy;

	cout << endl << "Decrement below limit and catch exception:" << endl;
	try
	{
		that_guy.decrementGrade();
	}
	catch (Bureaucrat::GradeTooLowException& e)
	{
		cout << e.what() << endl;
	}

	cout << endl << "Increment over limit and catch exception:" << endl;
	try
	{
		chief.incrementGrade();
	}
	catch (Bureaucrat::GradeTooHighException& e)
	{
		cout << e.what() << endl;
	}

	cout << endl << "Try to create a bureaucrat with an invalid grade:" << endl;
	try
	{
		nope = new Bureaucrat("Nope", -50);
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}

	try
	{
		nope = new Bureaucrat("Nope", 450);
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}

	cout << endl << "Copy a Bureaucrat:" << endl;
	copy_chief = new Bureaucrat(chief);
	cout << "Original:" << endl << chief;
	cout << "Copy:" << endl << *copy_chief;
	delete copy_chief;
}
