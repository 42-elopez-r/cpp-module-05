/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/05 20:36:33 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/06 19:50:36 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include <sstream>

using std::ostringstream;

Bureaucrat::Bureaucrat(const string& name, int grade): _name(name)
{
	if (grade < 1)
		throw (GradeTooHighException(grade));
	else if (grade > 150)
		throw (GradeTooLowException(grade));

	_grade = grade;
}

Bureaucrat::Bureaucrat(const Bureaucrat& bureaucrat): _name(bureaucrat._name)
{
	*this = bureaucrat;
}

Bureaucrat::~Bureaucrat()
{

}

Bureaucrat&
Bureaucrat::operator=(const Bureaucrat& bureaucrat)
{
	_grade = bureaucrat._grade;

	return (*this);
}

const string&
Bureaucrat::getName() const
{
	return (_name);
}

int
Bureaucrat::getGrade() const
{
	return (_grade);
}

void
Bureaucrat::incrementGrade()
{
	if (_grade - 1 < 1)
		throw (GradeTooHighException(_grade - 1));
	_grade--;
}

void
Bureaucrat::decrementGrade()
{
	if (_grade + 1 > 150)
		throw (GradeTooLowException(_grade + 1));
	_grade++;
}

Bureaucrat::GradeTooHighException::GradeTooHighException(int badGrade)
{
	ostringstream ss;

	ss << "Grade " << badGrade << " is too high";
	_msg = ss.str();
}

const char*
Bureaucrat::GradeTooHighException::what() const throw()
{
	return (_msg.c_str());
}

Bureaucrat::GradeTooLowException::GradeTooLowException(int badGrade)
{
	ostringstream ss;

	ss << "Grade " << badGrade << " is too low";
	_msg = ss.str();
}

const char*
Bureaucrat::GradeTooLowException::what() const throw()
{
	return (_msg.c_str());
}

ostream&
operator<<(ostream& os, const Bureaucrat& bureaucrat)
{
	os << bureaucrat.getName() << ", bureaucrat grade " << bureaucrat.getGrade() << endl;
	return (os);
}
