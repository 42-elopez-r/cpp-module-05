/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/10 21:00:46 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/11 12:02:20 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"

#include <iostream>
#include <exception>

using std::cout; using std::endl;
using std::exception;

int
main()
{
	Bureaucrat chief("Chief", 5);
	Bureaucrat someone("Someone", 130);
	Form important_form("Important", 10, 7);
	Form trivial_form("Trivial", 140, 135);
	Form* important_copy;
	Form* aux;

	cout << "Bureaucrats:" << endl << chief << someone;
	cout << endl << "Forms:" << endl << important_form << trivial_form;

	cout << endl << "Someone will try to sign Trivial:" << endl;
	someone.signForm(trivial_form);
	cout << trivial_form;
	cout << endl << "Someone will try to sign Important:" << endl;
	someone.signForm(important_form);
	cout << important_form;

	cout << endl << "Important copy:" << endl;
	important_copy = new Form(important_form);
	cout << *important_copy;
	delete important_copy;
	
	cout << endl << "Construction with wrong grades:" << endl;
	try
	{
		aux = new Form("Will fail", 300, 20);
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}
	try
	{
		aux = new Form("Will fail", 20, -20);
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}

	return (0);
}
