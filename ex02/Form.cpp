/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 19:45:22 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/13 13:43:51 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

#include <sstream>
#include <iostream>

using std::ostringstream; using std::endl;

Form::Form(const string& name, int minGradeSign, int minGradeExecute): _name(name),
	_minGradeSign(minGradeSign), _minGradeExecute(minGradeExecute)
{
	if (minGradeSign < 1)
		throw (GradeTooHighException(minGradeSign));
	else if (minGradeSign > 150)
		throw (GradeTooLowException(minGradeSign));
	else if (minGradeExecute < 1)
		throw (GradeTooHighException(minGradeExecute));
	else if (minGradeExecute > 150)
		throw (GradeTooLowException(minGradeExecute));
	_isSigned = false;
}

Form::Form(const Form& form): _name(form._name),
	_minGradeSign(form._minGradeSign), _minGradeExecute(form._minGradeExecute)
{
	*this = form;
}

Form::~Form()
{

}

Form&
Form::operator=(const Form& form)
{
	_isSigned = form._isSigned;

	return (*this);
}

const string&
Form::getName() const
{
	return (_name);
}

int
Form::getMinGradeSign() const
{
	return (_minGradeSign);
}

int
Form::getMinGradeExecute() const
{
	return (_minGradeExecute);
}

bool
Form::getSigned() const
{
	return (_isSigned);
}

void
Form::beSigned(const Bureaucrat& bureaucrat)
{
	if (bureaucrat.getGrade() > _minGradeSign)
		throw (GradeTooLowException(bureaucrat.getGrade()));
	_isSigned = true;
}

bool
Form::isValidExecutor(const Bureaucrat& executor) const
{
	return (executor.getGrade() <= _minGradeExecute);
}

ostream&
operator<<(ostream& os, const Form& form)
{
	os << form.getName() << ", form with minimum sign grade ";
	os << form.getMinGradeSign() << " and minimum execution grade ";
	os << form.getMinGradeExecute();
	os << (form.getSigned() ? " [SIGNED]" : " [NOT SIGNED]") << endl;

	return (os);
}

Form::GradeTooHighException::GradeTooHighException(int badGrade)
{
	ostringstream ss;

	ss << "Grade " << badGrade << " is too high";
	_msg = ss.str();
}

const char*
Form::GradeTooHighException::what() const throw()
{
	return (_msg.c_str());
}

Form::GradeTooLowException::GradeTooLowException(int badGrade)
{
	ostringstream ss;

	ss << "Grade " << badGrade << " is too low";
	_msg = ss.str();
}

const char*
Form::GradeTooLowException::what() const throw()
{
	return (_msg.c_str());
}

const char*
Form::NotSignedException::what() const throw()
{
	return ("Form not signed");
}
