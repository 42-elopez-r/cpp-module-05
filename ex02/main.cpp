/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/12 20:45:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/13 14:00:15 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include <iostream>

using std::cout; using std::endl;

int
main()
{
	Bureaucrat chief("Chief", 5);
	Bureaucrat someone("Someone", 140);
	ShrubberyCreationForm forestForm("forest");
	RobotomyRequestForm pancreasForm("my pancreas");
	PresidentialPardonForm nobodyForm("nobody");

	cout << "Bureaucrats:" << endl << chief << someone;
	cout << endl << "Forms:" << endl << forestForm << pancreasForm << nobodyForm;

	cout << endl << "Try to execute a not signed form:" << endl;
	chief.executeForm(forestForm);

	cout << endl << "Sign form but try to execute it with low grade:" << endl;
	chief.signForm(forestForm);
	someone.executeForm(forestForm);

	cout << endl << "Execute ShrubberyCreationForm:" << endl;
	chief.executeForm(forestForm);

	cout << endl << "Sign and execute RobotomyRequestForm:" << endl;
	chief.signForm(pancreasForm);
	chief.executeForm(pancreasForm);

	cout << endl << "Sign and execute nobodyForm:" << endl;
	chief.signForm(nobodyForm);
	chief.executeForm(nobodyForm);

	return (0);
}
