/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 12:44:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/13 13:42:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"
#include <fstream>

using std::ofstream; using std::ios;

ShrubberyCreationForm::ShrubberyCreationForm(const string& target):
	Form("ShrubberyCreationForm", 145, 137)
{
	_target = target;
}

ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm& shrubberyCreationForm):
	Form(shrubberyCreationForm)
{
	_target = shrubberyCreationForm._target;
}

ShrubberyCreationForm::~ShrubberyCreationForm()
{

}

ShrubberyCreationForm&
ShrubberyCreationForm::operator=(const ShrubberyCreationForm& shrubberyCreationForm)
{
	Form::operator=(shrubberyCreationForm);
	_target = shrubberyCreationForm._target;

	return (*this);
}

void
ShrubberyCreationForm::execute(const Bureaucrat& executor) const
{
	ofstream f;

	if (!isValidExecutor(executor))
		throw GradeTooLowException(executor.getGrade());
	if (!getSigned())
		throw NotSignedException();

	f.open(_target + "_shrubbery", ios::out | ios::trunc);
	for (int i = 0; i < 5; i++)
	{
		f << "         &&& &&  & &&" << endl;
		f << "      && &\\/&\\|& ()|/ @, &&" << endl;
		f << "      &\\/(/&/&||/& /_/)_&/_&" << endl;
		f << "   &() &\\/&|()|/&\\/ '%\" & ()" << endl;
		f << "  &_\\_&&_\\ |& |&&/&__%_/_& &&" << endl;
		f << "&&   && & &| &| /& & % ()& /&&" << endl;
		f << " ()&_---()&\\&\\|&&-&&--%---()~" << endl;
		f << "     &&     \\|||" << endl;
		f << "             |||" << endl;
		f << "             |||" << endl;
		f << "             |||" << endl;
		f << "       , -=-~  .-^- _" << endl << endl << endl;
	}
	f.close();
}
