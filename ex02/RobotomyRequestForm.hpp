/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.hpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 19:27:10 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/11 19:52:53 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROBOTOMYREQUESTFORM_HPP
#define ROBOTOMYREQUESTFORM_HPP

#include "Form.hpp"

using std::string;

class RobotomyRequestForm: public Form
{
	private:
		string _target;

		RobotomyRequestForm();
	public:
		RobotomyRequestForm(const string& target);
		RobotomyRequestForm(const RobotomyRequestForm& robotomyRequestForm);
		~RobotomyRequestForm();
		RobotomyRequestForm& operator=(const RobotomyRequestForm& robotomyRequestForm);
		void execute(const Bureaucrat& executor) const;
};

#endif
